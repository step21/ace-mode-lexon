
ace.define("ace/mode/lexon_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules"], function(acequire, exports, module) {
    "use strict";

    var oop = acequire("../lib/oop");
    var TextHighlightRules = acequire("./text_highlight_rules").TextHighlightRules;

    var LexonHighlightRules = function () {

        var keywords = (
            "lexon|lex|clause|terms|contracts|may|pays|pay|appoints|to"
        );

        var keywordMapper = this.createKeywordMapper({
            "keyword": keywords
        }, "identifier");

        var decimalInteger = "(?:(?:[1-9]\\d*)|(?:0))";
        var hexInteger = "(?:0[xX][\\dA-Fa-f]+)";
        var integer = "(?:" + decimalInteger + "|" + hexInteger + ")\\b";

        this.$rules = {
            "start": [
                {
                    token: "comment",  // single line comment
                    regex: "/\s*comment\s*:.*$/i"
                }, {
                    token: "comment",  // multi line comment
                    regex: "/\s*comment\s*:.*$/i",
                    next: "comment"
                }, {
                    token: "string", // single line
                    regex: "(/[\“\"\‘\'][a-zA-Z_\s]+[\”\"\’\']/i)",
                }, {
                    token: "constant.numeric", // integer
                    regex: integer
                }, {
                    token: keywordMapper,
                    regex: "[a-zA-Z_$][a-zA-Z0-9_$]*\\b"
                }, {
                    token: "keyword.operator",
                    regex: "\\+|\\-|\\*|\\*\\*|\\/|\\|\\||&&|\\^|!|<|>|<=|=>|==|!=|="
                }, {
                    toke: "def",
                    regex: "/\b(is|be|certified)\b/i",
                }, {
                    token: "variable-3",
                    regex: "/(added|add|subtract|less|greater|equal|le|gt|or)/i"
                }, {
                    token: "variable-3",
                    regex: "/\s*[=+\-*/<>]\s*/i"
                }, {
                    token: "atom",
                    regex: "/(?:amount|person|key|time|data|asset|text)\b/i",
                }, {
                    token: "variable-2",
                    regex: "/\s*[a-z_']+\s*:/i",
                }, {
                    token: "punctuation",
                    regex: ",|:|;"
                }, {
                    token: "lparen",
                    regex: "[[({]"
                }, {
                    token: "rparen",
                    regex: "[\\])}]"
                }, {
                    token: "text",
                    regex: "\\s+"
                }
            ],
          
        };
    };

    oop.inherits(LexonHighlightRules, TextHighlightRules);

    exports.LexonHighlightRules = LexonHighlightRules;
});

ace.define("ace/mode/lexon",["require","exports","module","ace/lib/oop","ace/mode/text","ace/mode/lexon_highlight_rules"], function(acequire, exports, module) {
    "use strict";

    var oop = acequire("../lib/oop");
    var TextMode = acequire("./text").Mode;
    var LexonHighlightRules = acequire("./lexon_highlight_rules").LexonHighlightRules;

    var Mode = function () {
        this.HighlightRules = LexonHighlightRules;
    };
    oop.inherits(Mode, TextMode);

    (function () {
        this.$id = "ace/mode/lexon";
    }).call(Mode.prototype);

    exports.Mode = Mode;
});
